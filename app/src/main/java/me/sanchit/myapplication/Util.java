package me.sanchit.myapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;

/**
 * Created by AKG002 on 06-03-2017.
 */

public class Util {
    public static Dialog getDialog(Context context, String title,String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (TextUtils.isEmpty(title))
            builder.setTitle(title);
        if (TextUtils.isEmpty(message))
            builder.setMessage(message);
        return builder.create();
    }
}
