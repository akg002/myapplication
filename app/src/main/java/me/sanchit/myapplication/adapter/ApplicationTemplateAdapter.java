package me.sanchit.myapplication.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import me.sanchit.myapplication.R;
import me.sanchit.myapplication.interfaces.OnClickListener;
import me.sanchit.myapplication.model.ApplicationTemplate;

/**
 * Created by AKG002 on 06-03-2017.
 */

public class ApplicationTemplateAdapter extends RecyclerView.Adapter<ApplicationTemplateAdapter.ViewHolder> {
    Context context;
    ArrayList<ApplicationTemplate> list;
    private OnClickListener listener;

    public ApplicationTemplateAdapter(Context context,ArrayList<ApplicationTemplate> list,OnClickListener listener) {
        this.context = context;
        this.list =list;
        this.listener = listener;
    }

    @Override
    public ApplicationTemplateAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_application_template,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ApplicationTemplateAdapter.ViewHolder holder, final int position) {
        ApplicationTemplate template = list.get(position);
        holder.tvName.setText(template.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onClick(position);

            }
        });

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        ImageView ivImage;
        public ViewHolder(View itemView) {
            super(itemView);
            tvName= (TextView)itemView.findViewById(R.id.text);
            ivImage = (ImageView)itemView.findViewById(R.id.image);
        }
    }
}
