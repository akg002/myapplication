package me.sanchit.myapplication.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import me.sanchit.myapplication.ui.RecyclerListFragment;

/**
 * Created by akg002 on 13/3/17.
 */


public  class RecyclerPagerAdapter extends FragmentStatePagerAdapter {

    private int count;
    private Context context;
    private ArrayList<RecyclerListFragment> list;

    public RecyclerPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void setArgs(int count, Context context,String[] title){
        this.count = count;
        this.context = context;
        list = new ArrayList<>();
        for (int i=0;i<count;i++){
            list.add(RecyclerListFragment.newInstance(context,title[i]));
        }

    }

    public RecyclerListFragment getRecyclerFragment(int pos){
        return ((RecyclerListFragment)getItem(pos));
    }


    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return list.get(position).getTitle();
    }
}