package me.sanchit.myapplication.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import me.sanchit.myapplication.Const;
import me.sanchit.myapplication.R;
import me.sanchit.myapplication.interfaces.OnClickListener;
import me.sanchit.myapplication.model.ApplicationDetail;

/**
 * Created by akg002 on 13/3/17.
 */

public class ApplicationAdapter extends RecyclerView.Adapter<ApplicationAdapter.ViewHolder> {
        Context context;
        ArrayList<ApplicationDetail> list;
    private OnClickListener listener;

    public ApplicationAdapter(Context context,ArrayList<ApplicationDetail> list,OnClickListener listener) {
        this.context = context;
        this.list =list;
        this.listener = listener;
        }

@Override
public ApplicationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_application,parent,false);
        return new ViewHolder(v);
        }

@Override
public void onBindViewHolder(ApplicationAdapter.ViewHolder holder, final int position) {
        ApplicationDetail detail = list.get(position);

    holder.tvStatus.setText(detail.getStatus());
    if (detail.getStatus().equalsIgnoreCase(Const.STATUS_PENDING))
        holder.tvStatus.setTextColor(ContextCompat.getColor(context,R.color.colorPending));
    else if (detail.getStatus().equalsIgnoreCase(Const.STATUS_ACCEPTED))
        holder.tvStatus.setTextColor(ContextCompat.getColor(context,R.color.colorAccept));
    else
        holder.tvStatus.setTextColor(ContextCompat.getColor(context,R.color.colorReject));
    holder.tvSubject.setText(detail.getSubject());
    holder.tvDate.setText(new SimpleDateFormat("dd-MM-yyyy").format(detail.getTimestamp()));
    holder.tvFacultyName.setText(detail.getFacultyName());
    holder.tvStudentName.setText(detail.getStudentName());
    if (detail.getFwdFacKey()!=null) {
        holder.tvForwarded.setVisibility(View.VISIBLE);
        holder.tvForwarded.setText("Forwarded to : "+detail.getFwdFacultyName());
    }
    else
        holder.tvForwarded.setVisibility(View.GONE);

    holder.itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            listener.onClick(position);
        }
    });

        }

@Override
public int getItemCount() {
        return list.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder {
    TextView tvStudentName,tvFacultyName,tvDate,tvSubject,tvStatus,tvForwarded;

    public ViewHolder(View itemView) {
        super(itemView);
        tvStudentName= (TextView)itemView.findViewById(R.id.tv_student_name);
        tvFacultyName= (TextView)itemView.findViewById(R.id.tv_faculty_name);
        tvDate= (TextView)itemView.findViewById(R.id.tv_date);
        tvSubject= (TextView)itemView.findViewById(R.id.tv_subject);
        tvStatus= (TextView)itemView.findViewById(R.id.tv_status);
        tvForwarded = (TextView)itemView.findViewById(R.id.tv_forwarded);

    }
}
}
