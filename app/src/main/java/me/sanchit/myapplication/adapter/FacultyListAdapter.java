package me.sanchit.myapplication.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import me.sanchit.myapplication.R;
import me.sanchit.myapplication.interfaces.OnClickListener;
import me.sanchit.myapplication.model.Faculty;

/**
 * Created by akg002 on 12/3/17.
 */

public class FacultyListAdapter extends RecyclerView.Adapter<FacultyListAdapter.VH> {
    Context context;
    ArrayList<Faculty> list;
    OnClickListener listener;

    public FacultyListAdapter(Context context, ArrayList<Faculty> list,OnClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    @Override
    public FacultyListAdapter.VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_faculty,parent,false);
        return new FacultyListAdapter.VH(v);
    }

    @Override
    public void onBindViewHolder(final FacultyListAdapter.VH holder, final int position) {
        holder.tvID.setText(list.get(position).getId());
        holder.tvName.setText(list.get(position).getName());
        holder.tvRole.setText(list.get(position).getRole());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onClick(holder.getAdapterPosition());

            }
        });
    }



    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VH extends RecyclerView.ViewHolder {
        TextView tvID,tvName,tvRole;
        public VH(View itemView) {
            super(itemView);
            tvID = (TextView)itemView.findViewById(R.id.tv_id);
            tvName = (TextView)itemView.findViewById(R.id.tv_name);
            tvRole = (TextView)itemView.findViewById(R.id.tv_role);
        }
    }
}
