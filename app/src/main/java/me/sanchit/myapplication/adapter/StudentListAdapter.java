package me.sanchit.myapplication.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import me.sanchit.myapplication.Const;
import me.sanchit.myapplication.R;
import me.sanchit.myapplication.interfaces.OnClickListener;
import me.sanchit.myapplication.model.Student;

/**
 * Created by AKG002 on 08-03-2017.
 */

public class StudentListAdapter extends RecyclerView.Adapter<StudentListAdapter.VH> {
    Context context;
    ArrayList<Student> list;
    OnClickListener listener;

    public StudentListAdapter(Context context, ArrayList<Student> list, OnClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }


    @Override
    public StudentListAdapter.VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_student, parent, false);
        return new VH(v);
    }

    @Override
    public void onBindViewHolder(final StudentListAdapter.VH holder, final int position) {
        holder.tvID.setText(list.get(position).getId());
        holder.tvName.setText(list.get(position).getName());
        if (list.get(position).getStatus().equalsIgnoreCase(Const.STATUS_ACCEPTED)){
            holder.tvStatus.setTextColor(ContextCompat.getColor(context,R.color.colorAccent));
        }
        else {
            holder.tvStatus.setTextColor(ContextCompat.getColor(context,R.color.colorReject));
        }
        holder.tvStatus.setText(list.get(position).getStatus());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.onClick(holder.getAdapterPosition());

            }
        });
    }



    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class VH extends RecyclerView.ViewHolder {
        TextView tvID, tvName, tvStatus;

        public VH(View itemView) {
            super(itemView);
            tvID = (TextView) itemView.findViewById(R.id.tv_id);
            tvName = (TextView) itemView.findViewById(R.id.tv_name);
            tvStatus = (TextView) itemView.findViewById(R.id.tv_status);


        }
    }
}
