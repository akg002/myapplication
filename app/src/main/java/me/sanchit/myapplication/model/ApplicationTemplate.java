package me.sanchit.myapplication.model;

import java.io.Serializable;

/**
 * Created by akg002 on 15/3/17.
 */

public class ApplicationTemplate implements Serializable{
    String subject;
    String body;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String name;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
