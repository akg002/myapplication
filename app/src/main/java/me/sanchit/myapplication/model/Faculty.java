package me.sanchit.myapplication.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by akg002 on 12/3/17.
 */
@IgnoreExtraProperties
public class Faculty extends User implements Serializable {
    public Faculty() {

    }

    String name;
    String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Map<String,ApplicationDetail> applications = new HashMap<>();

    @Exclude
    public String key;

    @Exclude
    public static ArrayList<Faculty> facultyList = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public  String getDestAdd(){
        return getName()+"\n"+getRole()+"\nAKGEC Ghaziabad";
    }
}
