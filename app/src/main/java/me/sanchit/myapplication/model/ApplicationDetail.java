package me.sanchit.myapplication.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

/**
 * Created by akg002 on 15/3/17.
 */


@IgnoreExtraProperties
public class ApplicationDetail implements Serializable {

    public String key;

    public String getStudentKey() {
        return studentKey;
    }

    public void setStudentKey(String studentKey) {
        this.studentKey = studentKey;
    }

    public String getFacultyKey() {
        return facultyKey;
    }

    public void setFacultyKey(String facultyKey) {
        this.facultyKey = facultyKey;
    }

    public String studentName;
    public String facultyName;
    public String studentKey;
    public String facultyKey;
    public String status;
    public long timestamp;
    public String fwdFacKey;

    public String getFwdFacKey() {
        return fwdFacKey;
    }

    public void setFwdFacKey(String fwdFacKey) {
        this.fwdFacKey = fwdFacKey;
    }

    public String getFwdFacultyName() {
        return fwdFacultyName;
    }

    public void setFwdFacultyName(String fwdFacultyName) {
        this.fwdFacultyName = fwdFacultyName;
    }

    public String getFwdFacultyRole() {
        return fwdFacultyRole;
    }

    public void setFwdFacultyRole(String fwdFacultyRole) {
        this.fwdFacultyRole = fwdFacultyRole;
    }

    public String fwdFacultyName;
    public String fwdFacultyRole;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getFacultyName() {
        return facultyName;
    }

    public void setFacultyName(String facultyName) {
        this.facultyName = facultyName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String subject;

    @Override
    public boolean equals(Object obj) {
        if (this.key.equalsIgnoreCase(((ApplicationDetail) obj).key))
            return true;
        return false;
    }
}
