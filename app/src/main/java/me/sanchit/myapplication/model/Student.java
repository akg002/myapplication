package me.sanchit.myapplication.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by AKG002 on 04-03-2017.
 */
@IgnoreExtraProperties
public class Student extends User implements Serializable{
    public Student() {

    }

    public String name;

    public int yr;
    public String branch;
    public String status;

    public Map<String,ApplicationDetail> applications = new HashMap<>();
    @Exclude
    public String key;


    @Exclude
    public static ArrayList<Student> studentsList = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getYr() {
        return yr;
    }

    public void setYr(int yr) {
        this.yr = yr;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getSignature(){
        return getName()+"\n"+getId()+"\n"+getYr()+"YR\t"+getBranch();
    }
}
