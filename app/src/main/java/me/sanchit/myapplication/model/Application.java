package me.sanchit.myapplication.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

/**
 * Created by AKG002 on 06-03-2017.
 */
@IgnoreExtraProperties
public class Application extends ApplicationDetail implements Serializable{

    private String destination;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String body;
    public String signature;


    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }


    public Application() {
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination){
        this.destination = destination;
    }
}
