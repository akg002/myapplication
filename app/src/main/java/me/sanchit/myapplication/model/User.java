package me.sanchit.myapplication.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

/**
 * Created by AKG002 on 06-03-2017.
 */
@IgnoreExtraProperties
public class User implements Serializable{
    public String password;
    public String id;

    public User() {
    }

    @Override
    public boolean equals(Object obj) {
        if(this.getId().equalsIgnoreCase(((User)obj).getId()))
            return true;
        else
            return false;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
