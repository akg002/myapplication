package me.sanchit.myapplication.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import me.sanchit.myapplication.Const;
import me.sanchit.myapplication.R;

public class OnBoardActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_board);
        findViewById(R.id.btn_admin).setOnClickListener(this);
        findViewById(R.id.btn_student).setOnClickListener(this);
        findViewById(R.id.btn_faculty).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this,LoginActivity.class);
        switch (v.getId()){
            case R.id.btn_admin:
                intent.putExtra("login", Const.ADMIN);
                break;
            case R.id.btn_student:
                intent.putExtra("login",Const.STUDENT);
                break;
            case R.id.btn_faculty:
                intent.putExtra("login",Const.FACULTY);
                break;
        }
        startActivity(intent);
    }
}
