package me.sanchit.myapplication.ui;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.sanchit.myapplication.R;

public class RecyclerListFragment extends Fragment {
RecyclerView recyclerView;
RecyclerView.LayoutManager layoutManager;
    private View emptyView;

    public void setTitle(CharSequence title) {
        this.title = title;
    }

    private CharSequence title = "";

    public void setAdapter(final RecyclerView.Adapter ad) {
        this.adapter = ad;
        if (recyclerView!=null) {
            recyclerView.setAdapter(adapter);
            if (adapter.getItemCount()>0)
                recyclerView.scrollToPosition(0);
            adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                @Override
                public void onChanged() {
                    super.onChanged();
                   checkAdapterStatus();
                }

                @Override
                public void onItemRangeRemoved(int positionStart, int itemCount) {
                    super.onItemRangeRemoved(positionStart, itemCount);
                    checkAdapterStatus();
                }
                @Override
                public void onItemRangeInserted(int positionStart, int itemCount) {
                    super.onItemRangeInserted(positionStart, itemCount);
                    checkAdapterStatus();
                }
            });
            checkAdapterStatus();
        }
    }

    public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
        this.layoutManager = layoutManager;
        if (recyclerView!=null)
            recyclerView.setLayoutManager(layoutManager);
    }

    RecyclerView.Adapter adapter;


    Context context;



    public RecyclerListFragment() {
        // Required empty public constructor
    }

    public static RecyclerListFragment newInstance(Context context,String title) {
        RecyclerListFragment fragment = new RecyclerListFragment();
        fragment.context = context;
        fragment.title = title;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recycler_list, container, false);
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        emptyView = view.findViewById(R.id.emptyView);
        if (adapter!=null) {
            recyclerView.setAdapter(adapter);
            if (adapter.getItemCount()>0)
            recyclerView.scrollToPosition(0);
            adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                @Override
                public void onChanged() {
                    super.onChanged();
                   checkAdapterStatus();
                }

                @Override
                public void onItemRangeRemoved(int positionStart, int itemCount) {
                    super.onItemRangeRemoved(positionStart, itemCount);
                    checkAdapterStatus();
                }

                @Override
                public void onItemRangeInserted(int positionStart, int itemCount) {
                    super.onItemRangeInserted(positionStart, itemCount);
                    checkAdapterStatus();
                }
            });
            checkAdapterStatus();
        }
        if (layoutManager!=null)
            recyclerView.setLayoutManager(layoutManager);

        return view;
    }

    private void checkAdapterStatus() {
        if (adapter.getItemCount()==0){
            showEmptyView();
        }else {
            hideEmptyView();
        }
    }

    private void hideEmptyView() {
        emptyView.setVisibility(View.GONE);
    }

    private void showEmptyView() {
        emptyView.setVisibility(View.VISIBLE);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public CharSequence getTitle() {
        return title;
    }

}
