package me.sanchit.myapplication.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import me.sanchit.myapplication.adapter.FacultyListAdapter;
import me.sanchit.myapplication.adapter.RecyclerPagerAdapter;
import me.sanchit.myapplication.adapter.StudentListAdapter;
import me.sanchit.myapplication.Const;
import me.sanchit.myapplication.R;
import me.sanchit.myapplication.interfaces.OnClickListener;
import me.sanchit.myapplication.model.Faculty;
import me.sanchit.myapplication.model.Student;
import me.sanchit.myapplication.services.FirebaseBackgroundService;

public class AdminDashboardActivity extends AppCompatActivity {
    static String TAG = AdminDashboardActivity.class.getSimpleName();
    DatabaseReference ref;
    ArrayList<Student> studentsList = Student.studentsList;
    ArrayList<Faculty> facultyList = Faculty.facultyList;
    StudentListAdapter studentListAdapter;
    FacultyListAdapter facultyListAdapter;
    TabLayout tabLayout;
    ViewPager viewPager;
    RecyclerPagerAdapter pagerAdapter;
    private ChildEventListener facultyListener;
    private ChildEventListener studentListener;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_dashboard);
        setTitle("Admin");
        sharedPreferences = getSharedPreferences(getString(R.string.app_name),MODE_PRIVATE);
        if (sharedPreferences.getBoolean(Const.USER_LOGGED_IN,false)) {
            if (!sharedPreferences.getString(Const.TYPE,"").equalsIgnoreCase(Const.ADMIN)) {
                moveBackToOnBoard();
            }}else {moveBackToOnBoard();}
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        ref = FirebaseDatabase.getInstance().getReference();
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        pagerAdapter = new RecyclerPagerAdapter(getSupportFragmentManager());
        studentListAdapter = new StudentListAdapter(this, studentsList, new OnClickListener() {
            @Override
            public void onClick(int pos) {
                showStudentInfo(pos);
            }
        });
        facultyListAdapter = new FacultyListAdapter(this, facultyList, new OnClickListener() {
            @Override
            public void onClick(int pos) {
                showFacultyInfo(pos);
            }
        });
        String[] tabTitles = {"Registrations","Faculties"};
        pagerAdapter.setArgs(2,this,tabTitles);
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        studentsList.clear();
        facultyList.clear();
        setUpStudentList();
        setUpFacultyList();

    }

    private void moveBackToOnBoard() {
        Intent intent = new Intent(this,SplashActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        pagerAdapter.getRecyclerFragment(0).setAdapter(studentListAdapter);
        pagerAdapter.getRecyclerFragment(1).setAdapter(facultyListAdapter);
        LinearLayoutManager lm1 = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,true);
        lm1.setStackFromEnd(true);
        LinearLayoutManager lm2 = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,true);
        lm2.setStackFromEnd(true);
        pagerAdapter.getRecyclerFragment(0).setLayoutManager(lm1);
        pagerAdapter.getRecyclerFragment(1).setLayoutManager(lm2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.admin_menu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.all_students:
                intent = new Intent(this,AllStudentListActivity.class);
                startActivity(intent);
                return true;
            case R.id.create_faculty:
                intent = new Intent(this,CreateFacultyActivity.class);
                startActivity(intent);
                return true;
            case R.id.logout:
                intent = new Intent(this,OnBoardActivity.class);
                getSharedPreferences(getString(R.string.app_name),MODE_PRIVATE).edit().clear().commit();
                stopService(new Intent(this,FirebaseBackgroundService.class));
                startActivity(intent);
                finish();
                return true;

        }
        return false;
    }

    private void showFacultyInfo(int pos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AdminDashboardActivity.this);
        builder.setView(R.layout.dialog_faculty_details);
        builder.setTitle("Faculty Details");
        final AlertDialog dialog = builder.create();
        dialog.show();
        final Faculty faculty = facultyList.get(pos);
        ((EditText)dialog.findViewById(R.id.et_id)).setText(faculty.getId());
        ((EditText)dialog.findViewById(R.id.et_name)).setText(faculty.getName());
        ((EditText)dialog.findViewById(R.id.et_password)).setText(faculty.getPassword());
        ((EditText)dialog.findViewById(R.id.et_role)).setText(faculty.getRole());
        final ProgressBar progressBar = (ProgressBar)dialog.findViewById(R.id.progressBar);
        progressBar.setIndeterminate(true);
        final Button update = ((Button)dialog.findViewById(R.id.btn_update));
        final Button cancel = ((Button)dialog.findViewById(R.id.btn_cancel));
        update.setEnabled(true);
        cancel.setEnabled(true);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                faculty.setPassword(((EditText)dialog.findViewById(R.id.et_password)).getText().toString());
                faculty.setRole(((EditText)dialog.findViewById(R.id.et_role)).getText().toString());
                progressBar.setVisibility(View.VISIBLE);
                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put( "/"+Const.PATH_USER+"/"+Const.FACULTY+"/"+faculty.key, faculty);
                dialog.setCancelable(false);
                update.setEnabled(false);
                cancel.setEnabled(false);
                ref.updateChildren(childUpdates, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        progressBar.setVisibility(View.GONE);
                        dialog.dismiss();
                        if (databaseError !=null)
                            Log.d(TAG,databaseError.toString());
                    }
                });
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        dialog.dismiss();
            }
        });
    }

    private void showStudentInfo(int pos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AdminDashboardActivity.this);
        builder.setView(R.layout.dialog_student_details);
        builder.setTitle("Student Details");
        final AlertDialog dialog = builder.create();
        dialog.show();
        final Student student = studentsList.get(pos);
        ((EditText)dialog.findViewById(R.id.et_id)).setText(student.getId());
        ((EditText)dialog.findViewById(R.id.et_name)).setText(student.getName());
        ((EditText)dialog.findViewById(R.id.et_branch)).setText(student.getBranch());
        ((EditText)dialog.findViewById(R.id.et_year)).setText(""+student.getYr());
        final ProgressBar progressBar = (ProgressBar)dialog.findViewById(R.id.progressBar);
        progressBar.setIndeterminate(true);
        final Button accept = ((Button)dialog.findViewById(R.id.btn_accept));
        final Button reject = ((Button)dialog.findViewById(R.id.btn_reject));
        accept.setEnabled(true);
        reject.setEnabled(true);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                student.setStatus(Const.STATUS_ACCEPTED);
                progressBar.setVisibility(View.VISIBLE);
                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put( student.key, student);
                dialog.setCancelable(false);
                accept.setEnabled(false);
                reject.setEnabled(false);
                ref.child(Const.PATH_USER).child(Const.STUDENT).updateChildren(childUpdates, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        progressBar.setVisibility(View.GONE);
                        dialog.dismiss();
                        if (databaseError !=null)
                            Log.d(TAG,databaseError.toString());
                    }
                });
            }
        });
        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                student.setStatus(Const.STATUS_REJECTED);
                progressBar.setVisibility(View.VISIBLE);
                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put( student.key, student);
                ref.child(Const.PATH_USER).child(Const.STUDENT).updateChildren(childUpdates, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        progressBar.setVisibility(View.GONE);
                        dialog.dismiss();
                        if (databaseError !=null)
                            Log.d(TAG,databaseError.toString());
                    }
                });
            }
        });


    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        if (ref!=null){
            if (facultyListener!=null)
                ref.removeEventListener(facultyListener);
            if (studentListener!=null)
                ref.removeEventListener(studentListener);
        }
    }

    private void setUpFacultyList() {
        facultyListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "Child Added"+dataSnapshot.toString());
                Faculty faculty = dataSnapshot.getValue(Faculty.class);
                faculty.key = dataSnapshot.getKey();
                facultyList.add(faculty);
                faculty.key = dataSnapshot.getKey();
                facultyListAdapter.notifyItemInserted(facultyList.size()-1);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG,"Child changed "+ dataSnapshot.toString());
                Faculty faculty = dataSnapshot.getValue(Faculty.class);
                faculty.key = dataSnapshot.getKey();
                int pos = facultyList.lastIndexOf(faculty);
                facultyList.set(pos,faculty);
                facultyListAdapter.notifyItemChanged(pos);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d(TAG,"Child removed "+ dataSnapshot.toString());
                Faculty faculty = dataSnapshot.getValue(Faculty.class);
                faculty.key = dataSnapshot.getKey();
                int pos = facultyList.lastIndexOf(faculty);
                facultyList.remove(pos);
                facultyListAdapter.notifyItemRemoved(pos);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, dataSnapshot.toString());
                Faculty faculty = dataSnapshot.getValue(Faculty.class);
                faculty.key = dataSnapshot.getKey();
                int pos = facultyList.lastIndexOf(faculty);
                facultyList.set(pos,faculty);
                facultyListAdapter.notifyItemChanged(pos);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, databaseError.toString());
            }
        };
        ref.child(Const.PATH_USER).child(Const.FACULTY)
                .orderByKey()
                .addChildEventListener(facultyListener);
    }

    private void setUpStudentList() {
studentListener = new ChildEventListener() {
    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        Log.d(TAG, "Child Added"+dataSnapshot.toString());
        Student student = dataSnapshot.getValue(Student.class);
        student.key = dataSnapshot.getKey();
        studentsList.add(student);
        studentListAdapter.notifyItemInserted(studentsList.size()-1);
    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
        Log.d(TAG,"Child changed "+ dataSnapshot.toString());
        Student student = dataSnapshot.getValue(Student.class);
        int pos = studentsList.lastIndexOf(student);
        studentsList.set(pos,student);
        studentListAdapter.notifyItemChanged(pos);

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {
        Log.d(TAG,"Child removed "+ dataSnapshot.toString());
        Student student = dataSnapshot.getValue(Student.class);
        int pos = studentsList.lastIndexOf(student);
        if (pos!=-1) {
            studentsList.remove(pos);
            Log.d(TAG,"Student removed from list"+ pos);
            studentListAdapter.notifyItemRemoved(pos);
           // studentListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
        Log.d(TAG, dataSnapshot.toString());
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        Log.d(TAG, databaseError.toString());
    }
} ;
        ref.child(Const.PATH_USER).child(Const.STUDENT)
                .orderByChild(Const.STATUS).equalTo(Const.STATUS_PENDING)
                .addChildEventListener(studentListener);
    }

}
