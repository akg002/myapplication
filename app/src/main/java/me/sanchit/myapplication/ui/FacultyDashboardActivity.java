package me.sanchit.myapplication.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collection;

import me.sanchit.myapplication.Const;
import me.sanchit.myapplication.R;
import me.sanchit.myapplication.adapter.ApplicationAdapter;
import me.sanchit.myapplication.interfaces.OnClickListener;
import me.sanchit.myapplication.model.ApplicationDetail;
import me.sanchit.myapplication.model.Faculty;
import me.sanchit.myapplication.services.FirebaseBackgroundService;

public class FacultyDashboardActivity extends AppCompatActivity {

    private static final String TAG = FacultyDashboardActivity.class.getSimpleName();
    private ChildEventListener applicationsListener;
    private DatabaseReference ref;
    Faculty faculty;
    private ArrayList<ApplicationDetail> applicaitonsList = new ArrayList<>();
    private ApplicationAdapter applicationAdapter;
    RecyclerView recyclerView;
    private View emptyView;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faculty_dashboard);
        setTitle("Faculty");
        sharedPreferences = getSharedPreferences(getString(R.string.app_name),MODE_PRIVATE);
        if (sharedPreferences.getBoolean(Const.USER_LOGGED_IN,false)) {
            if (!sharedPreferences.getString(Const.TYPE,"").equalsIgnoreCase(Const.FACULTY)) {
                moveBackToOnBoard();
            }}else {moveBackToOnBoard();}
        faculty = (Faculty) getIntent().getSerializableExtra(Const.FACULTY);
        ref = FirebaseDatabase.getInstance().getReference();
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);

        applicationAdapter = new ApplicationAdapter(this, applicaitonsList, new OnClickListener() {
            @Override
            public void onClick(int pos) {
                viewApplication(applicaitonsList.get(pos));
            }
        });
        LinearLayoutManager lm1 = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,true);
        lm1.setStackFromEnd(true);
        recyclerView.setLayoutManager(lm1);
        recyclerView.setAdapter(applicationAdapter);
        emptyView = findViewById(R.id.emptyView);
        applicationAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                checkAdapterStatus();
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                super.onItemRangeRemoved(positionStart, itemCount);
                checkAdapterStatus();
            }

            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                checkAdapterStatus();
            }
        });
        checkAdapterStatus();
        setUpApplicationList();


    }

    private void moveBackToOnBoard() {
        Intent intent = new Intent(this,SplashActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.faculty_menu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.logout:
                intent = new Intent(this,OnBoardActivity.class);
                getSharedPreferences(getString(R.string.app_name),MODE_PRIVATE).edit().clear().commit();
                stopService(new Intent(this,FirebaseBackgroundService.class));
                startActivity(intent);
                finish();
                return true;

        }
        return false;
    }



    private void checkAdapterStatus() {
        if (applicationAdapter.getItemCount()==0){
            showEmptyView();
        }else {
            hideEmptyView();
        }
    }

    private void hideEmptyView() {
        emptyView.setVisibility(View.GONE);
    }

    private void showEmptyView() {
        emptyView.setVisibility(View.VISIBLE);
    }


    private void viewApplication(ApplicationDetail applicationDetail) {
        Intent intent = new Intent(this,ApplicationActivity.class);
        intent.putExtra("mode","view");
        intent.putExtra("application",applicationDetail);
        intent.putExtra("login",Const.FACULTY);
        startActivity(intent);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        if (ref!=null && applicationsListener !=null)
            ref.removeEventListener(applicationsListener);
    }



    public void setUpApplicationList(){
        applicationsListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "Child Added"+dataSnapshot.toString());
                ApplicationDetail item = dataSnapshot.getValue(ApplicationDetail.class);
                applicaitonsList.add(item);
                applicationAdapter.notifyItemInserted(applicaitonsList.size()-1);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "Child CHanged"+dataSnapshot.toString());
                ApplicationDetail applicationDetail = dataSnapshot.getValue(ApplicationDetail.class);
                int pos = applicaitonsList.lastIndexOf(applicationDetail);
                applicaitonsList.set(pos,applicationDetail);
                applicationAdapter.notifyItemChanged(pos);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d(TAG, "Child rem"+dataSnapshot.toString());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "Child mov"+dataSnapshot.toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "Child canc"+databaseError.toString());
            }
        };
        ref.child(Const.PATH_USER).child(Const.FACULTY).child(faculty.key).child(Const.APPLICATIONS).orderByChild(Const.TIMESTAMP).addChildEventListener(applicationsListener);
    }
}
