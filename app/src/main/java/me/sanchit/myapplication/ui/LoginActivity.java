package me.sanchit.myapplication.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.HashMap;

import me.sanchit.myapplication.Const;
import me.sanchit.myapplication.R;
import me.sanchit.myapplication.model.Faculty;
import me.sanchit.myapplication.model.Student;
import me.sanchit.myapplication.model.User;
import me.sanchit.myapplication.services.FirebaseBackgroundService;

public class LoginActivity extends AppCompatActivity implements ValueEventListener {
    public String TAG = LoginActivity.class.getSimpleName();
    public String loginMode;
    EditText etId, etPassword;
    Button btnLogin, btnRegister;
    DatabaseReference databaseReference;
    ProgressDialog dialog;
    SharedPreferences.Editor spEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        spEditor = getSharedPreferences(getString(R.string.app_name),MODE_PRIVATE).edit();

        etId = (EditText) findViewById(R.id.et_id);
        etPassword = (EditText) findViewById(R.id.et_password);
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnRegister = (Button) findViewById(R.id.btn_register);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initiateLogin();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Login");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return false;
    }

    @Override
    protected void onResume(){
        super.onResume();
        loginMode = getIntent().getStringExtra("login");

        if (loginMode.equalsIgnoreCase(Const.STUDENT)) {
            //etId.setHint("University Roll No.");
            btnRegister.setVisibility(View.VISIBLE);
        }else {
            btnRegister.setVisibility(View.GONE);
        }
        databaseReference = FirebaseDatabase.getInstance().getReference(Const.PATH_USER).child(loginMode);

    }

    @Override
    protected void onStop() {
        super.onStop();
        databaseReference.removeEventListener(this);
    }

    private void initiateLogin() {
        dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Loading");
        dialog.show();
        databaseReference
                .orderByChild("id")
                .equalTo(etId.getText().toString())
                .addListenerForSingleValueEvent(this);
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        Log.d(TAG, dataSnapshot.toString());
        dialog.dismiss();
        if (dataSnapshot.getValue() != null && dataSnapshot.getChildrenCount() == 1) {
            for (DataSnapshot ds : dataSnapshot.getChildren()) {
                User user = ds.getValue(User.class);
                if (user.getPassword().equalsIgnoreCase(etPassword.getText().toString())) {
                    Log.d(TAG, "Login successful");
                    moveToDashBoard(ds);
                    return;
                }
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle("Error");
        builder.setMessage("Invalid credentials");
        builder.show();
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        Log.d(TAG, databaseError.toString());
        dialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle("Error");
        builder.setMessage("Some error occurred");
        builder.show();
    }

    private void moveToDashBoard(DataSnapshot ds) {
        Intent intent;
        if (loginMode.equalsIgnoreCase(Const.ADMIN)) {
            intent = new Intent(this, AdminDashboardActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            spEditor.putBoolean(Const.USER_LOGGED_IN,true);
            spEditor.putString(Const.TYPE,Const.ADMIN);
            spEditor.commit();
            startService(new Intent(this,FirebaseBackgroundService.class));
            startActivity(intent);


        } else if (loginMode.equalsIgnoreCase(Const.FACULTY)) {
            intent = new Intent(this, FacultyDashboardActivity.class);
            Faculty faculty = ds.getValue(Faculty.class);
            faculty.key = ds.getKey();
            intent.putExtra(Const.FACULTY,faculty);
            spEditor.putBoolean(Const.USER_LOGGED_IN,true);
            spEditor.putString(Const.TYPE,Const.FACULTY);
            Gson gson = new Gson();
            spEditor.putString(Const.FACULTY,gson.toJson(faculty));
            spEditor.commit();
            startService(new Intent(this,FirebaseBackgroundService.class));
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);


        }
        else {
            Student student = ds.getValue(Student.class);
            student.key = ds.getKey();
            if (student.getStatus().equalsIgnoreCase(Const.STATUS_PENDING)){
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle("Alert");
                builder.setMessage("Account not verified");
                builder.show();
            }else if (student.getStatus().equalsIgnoreCase(Const.STATUS_REJECTED)){
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle("Alert");
                builder.setMessage("Account rejected ");
                builder.show();
            }else{
                intent = new Intent(this, StudentDashboardActivity.class);
               /* intent.putExtra(Const.KEY,student.key);
                intent.putExtra(Const.ID,student.getId());
                intent.putExtra(Const.STUDENT_NAME,student.getName());
                intent.putExtra(Const.STUDENT_YEAR,student.getYr());
                intent.putExtra(Const.STUDENT_BRANCH,student.getBranch());*/
               intent.putExtra(Const.STUDENT,student);
                spEditor.putBoolean(Const.USER_LOGGED_IN,true);
                spEditor.putString(Const.TYPE,Const.STUDENT);
                Gson gson = new Gson();
                spEditor.putString(Const.STUDENT,gson.toJson(student));
                spEditor.commit();
                startService(new Intent(this,FirebaseBackgroundService.class));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        }
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    }
}
