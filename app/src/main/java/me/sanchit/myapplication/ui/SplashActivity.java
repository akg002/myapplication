package me.sanchit.myapplication.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.gson.Gson;

import me.sanchit.myapplication.Const;
import me.sanchit.myapplication.R;
import me.sanchit.myapplication.model.Faculty;
import me.sanchit.myapplication.model.Student;
import me.sanchit.myapplication.services.FirebaseBackgroundService;

public class SplashActivity extends AppCompatActivity {
SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharedPreferences = getSharedPreferences(getString(R.string.app_name),MODE_PRIVATE);
        Intent intent =new Intent(SplashActivity.this, OnBoardActivity.class);;

        if (sharedPreferences.getBoolean(Const.USER_LOGGED_IN,false)) {

            if (sharedPreferences.getString(Const.TYPE,"").equalsIgnoreCase(Const.ADMIN)) {
                intent = new Intent(SplashActivity.this, AdminDashboardActivity.class);
            }else if (sharedPreferences.getString(Const.TYPE,"").equalsIgnoreCase(Const.FACULTY)) {
                intent = new Intent(SplashActivity.this, FacultyDashboardActivity.class);
                Gson gson = new Gson();
                Faculty faculty = gson.fromJson(sharedPreferences.getString(Const.FACULTY,null),Faculty.class);
                intent.putExtra(Const.FACULTY,faculty);
            }else if (sharedPreferences.getString(Const.TYPE,"").equalsIgnoreCase(Const.STUDENT)) {
                intent = new Intent(SplashActivity.this, StudentDashboardActivity.class);
                Gson gson = new Gson();
                Student student = gson.fromJson(sharedPreferences.getString(Const.STUDENT,null),Student.class);
                intent.putExtra(Const.STUDENT,student);
            }
            startActivity(intent);
            startService(new Intent(this,FirebaseBackgroundService.class));
            finish();

        }else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashActivity.this, OnBoardActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, 1500);
        }

    }
}
