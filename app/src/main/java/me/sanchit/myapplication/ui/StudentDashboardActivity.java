package me.sanchit.myapplication.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONObject;

import java.util.ArrayList;

import me.sanchit.myapplication.adapter.ApplicationAdapter;
import me.sanchit.myapplication.adapter.ApplicationTemplateAdapter;
import me.sanchit.myapplication.adapter.RecyclerPagerAdapter;
import me.sanchit.myapplication.Const;
import me.sanchit.myapplication.R;
import me.sanchit.myapplication.interfaces.OnClickListener;
import me.sanchit.myapplication.model.ApplicationDetail;
import me.sanchit.myapplication.model.ApplicationTemplate;
import me.sanchit.myapplication.model.Student;
import me.sanchit.myapplication.services.FirebaseBackgroundService;

public class StudentDashboardActivity extends AppCompatActivity {
    private static final String TAG = StudentDashboardActivity.class.getSimpleName();
    Student student;
    Bundle incomingBundle;
    TabLayout tabLayout;
    ViewPager viewPager;
    RecyclerPagerAdapter pagerAdapter;
    ApplicationTemplateAdapter templateAdapter;
    ApplicationAdapter applicationAdapter;
    ArrayList<ApplicationTemplate> templateArrayList ;
    private DatabaseReference ref;
    private ArrayList<ApplicationDetail> applicaitonsList;
    private ChildEventListener applicationsListener;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_dashboard);
        setTitle("Student");
        sharedPreferences = getSharedPreferences(getString(R.string.app_name),MODE_PRIVATE);
        if (sharedPreferences.getBoolean(Const.USER_LOGGED_IN,false)) {
            if (!sharedPreferences.getString(Const.TYPE,"").equalsIgnoreCase(Const.STUDENT)) {
                moveBackToOnBoard();
            }}else {moveBackToOnBoard();}
        /*incomingBundle= getIntent().getExtras();
        student = new Student();
        student.key = getIntent().getStringExtra(Const.KEY);
        student.setId(getIntent().getStringExtra(Const.ID));
        student.setName(getIntent().getStringExtra(Const.STUDENT_NAME));
        student.setBranch(getIntent().getStringExtra(Const.STUDENT_BRANCH));
        student.setYr(getIntent().getIntExtra(Const.STUDENT_YEAR,1));*/
        student = (Student) getIntent().getSerializableExtra(Const.STUDENT);
        applicaitonsList = new ArrayList<>();

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        ref = FirebaseDatabase.getInstance().getReference();
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        pagerAdapter = new RecyclerPagerAdapter(getSupportFragmentManager());
        String[] tabTitles = {"Status","Template"};
        pagerAdapter.setArgs(2,this,tabTitles);
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        setUpTemplateList();
        templateAdapter = new ApplicationTemplateAdapter(this, templateArrayList, new OnClickListener() {
            @Override
            public void onClick(int pos) {
                editTemplate(templateArrayList.get(pos));
            }
        });
        applicationAdapter = new ApplicationAdapter(this, applicaitonsList, new OnClickListener() {
            @Override
            public void onClick(int pos) {
                viewApplication(applicaitonsList.get(pos));
            }
        });
        setUpApplicationList();
        LinearLayoutManager lm1 = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,true);
        lm1.setStackFromEnd(true);
        pagerAdapter.getRecyclerFragment(1).setAdapter(templateAdapter);
        pagerAdapter.getRecyclerFragment(1).setLayoutManager(new GridLayoutManager(this,2));
        pagerAdapter.getRecyclerFragment(0).setAdapter(applicationAdapter);
        pagerAdapter.getRecyclerFragment(0).setLayoutManager(lm1);
    }

    private void moveBackToOnBoard() {
        Intent intent = new Intent(this,SplashActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.student_menu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()){
            case R.id.logout:
                intent = new Intent(this,OnBoardActivity.class);
                getSharedPreferences(getString(R.string.app_name),MODE_PRIVATE).edit().clear().commit();
                stopService(new Intent(this,FirebaseBackgroundService.class));
                startActivity(intent);
                finish();
                return true;
        }
        return false;
    }



    private void viewApplication(ApplicationDetail applicationDetail) {
        Intent intent = new Intent(this,ApplicationActivity.class);
        intent.putExtra("mode","view");
        intent.putExtra("application",applicationDetail);
        intent.putExtra("login",Const.STUDENT);
        intent.putExtra(Const.STUDENT,student);
        startActivity(intent);
    }

    private void editTemplate(ApplicationTemplate applicationTemplate) {
        Intent intent = new Intent(this,ApplicationActivity.class);
        intent.putExtra("mode","edit");
        intent.putExtra("template",applicationTemplate);
        intent.putExtra(Const.STUDENT,student);
        startActivity(intent);

    }

    private void setUpTemplateList() {
        templateArrayList = new ArrayList<>();
        String[] array = this.getResources().getStringArray(R.array.application_template);
        for (String item :array){
            try {
                Log.d("json",item);
                JSONObject jsonObject = new JSONObject(item);
                ApplicationTemplate template = new ApplicationTemplate();
                template.setName(jsonObject.getString("name"));
                template.setSubject(jsonObject.getString("subject"));
                template.setBody(jsonObject.getString("body"));
                templateArrayList.add(template);

            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        if (applicationsListener!=null && ref!=null){
            ref.removeEventListener(applicationsListener);
        }
    }

    public void setUpApplicationList(){
        applicationsListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "Child Added"+dataSnapshot.toString());
                ApplicationDetail item = dataSnapshot.getValue(ApplicationDetail.class);
                applicaitonsList.add(item);
                applicationAdapter.notifyItemInserted(applicaitonsList.size()-1);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "Child CHanged"+dataSnapshot.toString());
                ApplicationDetail applicationDetail = dataSnapshot.getValue(ApplicationDetail.class);
                int pos = applicaitonsList.lastIndexOf(applicationDetail);
                applicaitonsList.set(pos,applicationDetail);
                applicationAdapter.notifyItemChanged(pos);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d(TAG, "Child rem"+dataSnapshot.toString());
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "Child mov"+dataSnapshot.toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, "Child canc"+databaseError.toString());
            }
        };
        ref.child(Const.PATH_USER).child(Const.STUDENT).child(student.key).child(Const.APPLICATIONS).orderByChild(Const.TIMESTAMP).addChildEventListener(applicationsListener);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

}
