package me.sanchit.myapplication.ui;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import me.sanchit.myapplication.Const;
import me.sanchit.myapplication.R;
import me.sanchit.myapplication.model.Student;

public class RegisterActivity extends AppCompatActivity {
    DatabaseReference databaseReference;
EditText etName,etPassword,etUniRoll;
    Spinner spinnerBranch,spinnerYear;
    Button btnRegister;
    String[] branches,years;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        etName = (EditText)findViewById(R.id.et_name);
        etPassword = (EditText)findViewById(R.id.et_password);
        etUniRoll = (EditText)findViewById(R.id.et_id);
        spinnerBranch = (Spinner)findViewById(R.id.spinner_branch);
        spinnerYear = (Spinner)findViewById(R.id.spinner_year);
        btnRegister = (Button)findViewById(R.id.btn_register);
        branches = getResources().getStringArray(R.array.branches);
        years = getResources().getStringArray(R.array.years);

        databaseReference = FirebaseDatabase.getInstance().getReference(Const.PATH_USER).child(Const.STUDENT);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              initiateRegister();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Register");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return false;
    }

    public void initiateRegister(){
        final Student student = new Student();
        student.setName(etName.getText().toString());
        student.setPassword(etPassword.getText().toString());
        student.setId(etUniRoll.getText().toString());
        student.setBranch(branches[spinnerBranch.getSelectedItemPosition()]);
        student.setStatus(Const.STATUS_PENDING);
        student.setYr(Integer.parseInt(years[spinnerYear.getSelectedItemPosition()]));
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Loading");
        dialog.show();
        databaseReference
                .orderByChild("id")
                .equalTo(etUniRoll.getText().toString())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        dialog.dismiss();
                        if(dataSnapshot.getValue()==null){
                            registerStudent(student);
                        }else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                            builder.setTitle("Error");
                            builder.setMessage("Already registered!");
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builder.show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        dialog.dismiss();
                        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                        builder.setTitle("Error");
                        builder.setMessage(databaseError.getMessage());
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.show();

                    }
                });
    }

    private void registerStudent(Student student) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Loading");
        dialog.show();
        student.key = databaseReference.push().getKey();
        databaseReference.child(student.key)
                .setValue(student)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dialog.dismiss();
                        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                        builder.setTitle("Error");
                        builder.setMessage(e.getMessage());
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.show();
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dialog.dismiss();
                        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                        builder.setTitle("Success");
                        builder.setMessage("Your account created successfully");
                        builder.setCancelable(false);
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                moveToDashBoard();
                                dialog.dismiss();
                            }
                        });
                        builder.show();
                    }
                });
    }

    public void moveToDashBoard(){
        finish();
    }
}
