package me.sanchit.myapplication.ui;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import me.sanchit.myapplication.Const;
import me.sanchit.myapplication.R;
import me.sanchit.myapplication.adapter.FacultyListAdapter;
import me.sanchit.myapplication.interfaces.OnClickListener;
import me.sanchit.myapplication.model.Application;
import me.sanchit.myapplication.model.ApplicationDetail;
import me.sanchit.myapplication.model.ApplicationTemplate;
import me.sanchit.myapplication.model.Faculty;
import me.sanchit.myapplication.model.Student;

public class ApplicationActivity extends AppCompatActivity {

    private static final String TAG = ApplicationActivity.class.getSimpleName();
    LinedEditText etSubject, etBody, etDestination, etDate, etSignature, etFwdFacName, etFwdFacRole;
    DatabaseReference reference;
    int mode;
    ApplicationTemplate template;
    Application application;
    ApplicationDetail detail;
    Faculty faculty;
    long timestamp = 0;
    FloatingActionButton submit, approve, reject, forward, fwdToFac;
    ArrayList<Faculty> facultyList = new ArrayList<>();
    ChildEventListener facultyListener;
    LinearLayout viewFwdFaculty;
    ProgressDialog dialog;
    Faculty fwdFaculty;
    private Student student;
    private FacultyListAdapter facultyListAdapter;
    private AlertDialog facultyListDialog;
    private ApplicationDetail applicationDetail;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_application);
        reference = FirebaseDatabase.getInstance().getReference();
        sharedPreferences = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);
       /* student = new Student();
        student.key = getIntent().getStringExtra(Const.KEY);
        student.setId(getIntent().getStringExtra(Const.ID));
        student.setName(getIntent().getStringExtra(Const.STUDENT_NAME));
        student.setBranch(getIntent().getStringExtra(Const.STUDENT_BRANCH));
        student.setYr(getIntent().getIntExtra(Const.STUDENT_YEAR,1));*/
        etSubject = (LinedEditText) findViewById(R.id.et_subject);
        etBody = (LinedEditText) findViewById(R.id.et_body);
        etDate = (LinedEditText) findViewById(R.id.et_date);
        etDestination = (LinedEditText) findViewById(R.id.et_destination);
        etSignature = (LinedEditText) findViewById(R.id.et_signature);
        submit = (FloatingActionButton) findViewById(R.id.submit);
        approve = (FloatingActionButton) findViewById(R.id.approve);
        reject = (FloatingActionButton) findViewById(R.id.reject);
        forward = (FloatingActionButton) findViewById(R.id.forward);
        viewFwdFaculty = (LinearLayout) findViewById(R.id.view_fwd);
        etFwdFacName = (LinedEditText) findViewById(R.id.et_fwd_faculty);
        etFwdFacRole = (LinedEditText) findViewById(R.id.et_fwd_faculty_role);
        fwdToFac = (FloatingActionButton) findViewById(R.id.fwd_to_fac);

    }

    private void moveBackToOnBoard() {
        Intent intent = new Intent(this, SplashActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent().getStringExtra("mode").equalsIgnoreCase("edit")) {
            mode = 0;
            template = (ApplicationTemplate) getIntent().getSerializableExtra("template");
            etSubject.setText(template.getSubject());
            etBody.setText(template.getBody());
            student = (Student) getIntent().getSerializableExtra(Const.STUDENT);
            application = new Application();
            etSignature.setText(student.getSignature());
            final Calendar c = Calendar.getInstance();
            final int year = c.get(Calendar.YEAR);
            final int month = c.get(Calendar.MONTH);
            final int day = c.get(Calendar.DAY_OF_MONTH);
            timestamp = c.getTimeInMillis();
            //setDate(timestamp);
            etDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new DatePickerDialog(ApplicationActivity.this,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                    c.set(year, month, dayOfMonth);
                                    timestamp = c.getTimeInMillis();
                                    setDate(timestamp);
                                }
                            }, year, month, day
                    ).show();
                }
            });
            submit.setVisibility(View.VISIBLE);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    submitApplication();
                }
            });
            etDestination.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showFacultyList();
                }
            });

            setTitle("Edit Application");

        } else if (getIntent().getStringExtra("mode").equalsIgnoreCase("forward")) {
            application = (Application) getIntent().getSerializableExtra("application");
            faculty = null;
            application.setDestination(null);
            application.setTimestamp(0);
            application.setFwdFacKey(null);
            application.setFwdFacultyName(null);
            application.setFwdFacultyRole(null);
            student = (Student) getIntent().getSerializableExtra(Const.STUDENT);
            timestamp = 0;
            setUpApplicationView();
            forward.setVisibility(View.GONE);
            submit.setVisibility(View.VISIBLE);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    submitApplication();
                }
            });
            etDestination.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showFacultyList();
                }
            });
            setUpFacultyList();
            final Calendar c = Calendar.getInstance();
            final int year = c.get(Calendar.YEAR);
            final int month = c.get(Calendar.MONTH);
            final int day = c.get(Calendar.DAY_OF_MONTH);
            etDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new DatePickerDialog(ApplicationActivity.this,
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                    c.set(year, month, dayOfMonth);
                                    timestamp = c.getTimeInMillis();
                                    setDate(timestamp);
                                }
                            }, year, month, day
                    ).show();
                }
            });
            setTitle("Edit Application");

        } else {
            mode = 1;
            applicationDetail = (ApplicationDetail) getIntent().getSerializableExtra("application");
            loadApplication(applicationDetail);

            if (!sharedPreferences.getBoolean(Const.USER_LOGGED_IN, false)) {
                moveBackToOnBoard();
            }

            if (sharedPreferences.getString(Const.TYPE, "").equalsIgnoreCase(Const.STUDENT))
                student = (Student) getIntent().getSerializableExtra(Const.STUDENT);
            setTitle("View Application");
        }
        setUpFacultyList();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return false;
    }

    public void showDialog() {
    if (dialog ==null)
     dialog = new ProgressDialog(this);
    dialog.setMessage("Loading");
    dialog.show();
    }

    public void hideDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    void loadApplication(ApplicationDetail app) {
        showDialog();
        reference.child(Const.APPLICATIONS).child(app.key).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideDialog();
                application = dataSnapshot.getValue(Application.class);
                Log.d(TAG, dataSnapshot.toString());
                setUpApplicationView();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideDialog();
                Log.d(TAG, databaseError.toString());
            }
        });
    }

    private void setUpApplicationView() {
        etSubject.setText(application.getSubject());
        if (getIntent().getStringExtra("mode").equalsIgnoreCase("view"))
            etSubject.setFocusable(false);
        etBody.setText(application.getBody());
        if (getIntent().getStringExtra("mode").equalsIgnoreCase("view"))
            etBody.setFocusable(false);
        etSignature.setText(application.getSignature());
        if (application.getDestination() != null) {
            etDestination.setText(application.getDestination());
            etDestination.setTextColor(getResources().getColor(R.color.darkGray));
        }
        if (application.getTimestamp() != 0) {
            setDate(application.getTimestamp());
        }

        if (application.getFwdFacultyName() != null) {
            //make forwarded by view visible
            viewFwdFaculty.setVisibility(View.VISIBLE);
            etFwdFacName.setText(application.getFwdFacultyName());
            etFwdFacRole.setText(application.getFwdFacultyRole());
        } else
            viewFwdFaculty.setVisibility(View.GONE);

        if (sharedPreferences.getString(Const.TYPE, "").equalsIgnoreCase(Const.FACULTY)) {
            if (application.getStatus().equalsIgnoreCase(Const.STATUS_PENDING))
                showFacultyControls();
            showFwdToFacButton();
        } else if (sharedPreferences.getString(Const.TYPE, "").equalsIgnoreCase(Const.STUDENT))
            showForwardControl();
    }

    private void showFwdToFacButton() {
        fwdToFac.setVisibility(View.VISIBLE);
        fwdToFac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFacultyList();
            }
        });
    }

    private void showForwardControl() {
        forward.setVisibility(View.VISIBLE);
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ApplicationActivity.this, ApplicationActivity.class);
                // startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));
                intent.putExtra("application", application);
                intent.putExtra("mode", "forward");
                intent.putExtra("login", Const.STUDENT);
                intent.putExtra(Const.STUDENT, student);
                startActivity(intent);
            }
        });
    }

    private void showFacultyList() {
        if (facultyListDialog != null) {
            facultyListDialog.show();
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.view_recycler_view, null, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(facultyListAdapter);
        builder.setView(view);
        builder.setTitle("Faculty List");
        facultyListDialog = builder.create();
        facultyListDialog.show();
    }

    public void showFacultyControls() {
        approve.setVisibility(View.VISIBLE);
        approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                approveApplication();
            }
        });
        reject.setVisibility(View.VISIBLE);
        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rejectApplocation();
            }
        });
    }

    private void rejectApplocation() {
        application.setStatus(Const.STATUS_REJECTED);
        updateApplication(application);
    }

    private void updateApplication(Application application) {
        showDialog();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("/" + Const.APPLICATIONS + "/" + application.key, application);
        map.put("/" + Const.PATH_USER + "/" + Const.STUDENT + "/" + application.getStudentKey() + "/"
                + Const.APPLICATIONS + "/"
                + application.key, (ApplicationDetail) application);
        map.put("/" + Const.PATH_USER + "/" + Const.FACULTY + "/" + application.getFacultyKey() + "/"
                + Const.APPLICATIONS + "/"
                + application.key, (ApplicationDetail) application);

        if (application.getFwdFacKey() != null)
            map.put("/" + Const.PATH_USER + "/" + Const.FACULTY + "/" + application.getFwdFacKey() + "/"
                    + Const.APPLICATIONS + "/"
                    + application.key, (ApplicationDetail) application);

        reference.updateChildren(map).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                hideDialog();
                onBackPressed();
            }
        });
    }

    private void approveApplication() {
        application.setStatus(Const.STATUS_ACCEPTED);
        updateApplication(application);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (facultyListener != null)
            reference.removeEventListener(facultyListener);
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    private void setUpFacultyList() {
        if (facultyListener != null)
            return;
        facultyListAdapter = new FacultyListAdapter(this, facultyList, new OnClickListener() {
            @Override
            public void onClick(int pos) {
                if (sharedPreferences.getString(Const.TYPE, "").equalsIgnoreCase(Const.FACULTY)) {
                    fwdFaculty = facultyList.get(pos);
                    application.setFwdFacKey(fwdFaculty.key);
                    application.setFwdFacultyName(fwdFaculty.getName());
                    application.setFwdFacultyRole(fwdFaculty.getRole());
                    updateApplication(application);
                } else {
                    faculty = facultyList.get(pos);
                    etDestination.setText(faculty.getDestAdd());
                    etDestination.setTextColor(getResources().getColor(R.color.darkGray));
                }
                if (facultyListDialog.isShowing())
                    facultyListDialog.dismiss();
            }
        });
        facultyListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "Child Added" + dataSnapshot.toString());
                Faculty faculty = dataSnapshot.getValue(Faculty.class);
                facultyList.add(faculty);
                faculty.key = dataSnapshot.getKey();
                facultyListAdapter.notifyItemInserted(facultyList.size() - 1);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "Child changed " + dataSnapshot.toString());
                Faculty faculty = dataSnapshot.getValue(Faculty.class);
                int pos = facultyList.lastIndexOf(faculty);
                facultyList.set(pos, faculty);
                facultyListAdapter.notifyItemChanged(pos);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d(TAG, "Child removed " + dataSnapshot.toString());
                Faculty faculty = dataSnapshot.getValue(Faculty.class);
                int pos = facultyList.lastIndexOf(faculty);
                facultyList.remove(pos);
                facultyListAdapter.notifyItemRemoved(pos);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, dataSnapshot.toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, databaseError.toString());
            }
        };
        reference.child(Const.PATH_USER).child(Const.FACULTY)
                .orderByKey()
                .addChildEventListener(facultyListener);
    }


    public void submitApplication() {
        if (faculty == null) {
            Toast.makeText(this, "Select a faculty.", Toast.LENGTH_LONG).show();
            return;
        }
        if (timestamp == 0) {
            Toast.makeText(this, "Select a Date.", Toast.LENGTH_LONG).show();
            return;
        }

        try {
            String key = reference.child(Const.APPLICATIONS).push().getKey();
            application.key = key;
            application.setStatus(Const.STATUS_PENDING);
            application.setDestination(faculty.getDestAdd());
            application.setTimestamp(timestamp);
            application.setSubject(etSubject.getText().toString());
            application.setBody(etBody.getText().toString());
            application.setSignature(student.getSignature());
            application.setStudentName(student.getName());
            application.setStudentKey(student.key);
            application.setFacultyName(faculty.getName());
            application.setFacultyKey(faculty.key);
            student.applications.put(key, ((ApplicationDetail) application));
            faculty.applications.put(key, ((ApplicationDetail) application));
            updateApplication(application);
          /*  Map<String, Object> map = new HashMap<String, Object>();
            map.put("/" + Const.APPLICATIONS + "/" + key, application);
            map.put("/" + Const.PATH_USER + "/" + Const.STUDENT + "/" + student.key, student);
            map.put("/" + Const.PATH_USER + "/" + Const.FACULTY + "/" + faculty.key, faculty);
            reference.updateChildren(map);*/

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Something went wrong.", Toast.LENGTH_LONG).show();
        }
    }

    public void setDate(long timestamp) {
        etDate.setText(new SimpleDateFormat("dd-MM-yyyy").format(timestamp));
        etDate.setTextColor(getResources().getColor(R.color.darkGray));
    }
}
