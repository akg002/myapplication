package me.sanchit.myapplication.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import me.sanchit.myapplication.Const;
import me.sanchit.myapplication.R;
import me.sanchit.myapplication.model.Faculty;
import me.sanchit.myapplication.model.Student;

public class CreateFacultyActivity extends AppCompatActivity {
    AppCompatEditText etName,etId,etRole,etPassword;
    Button btnCreate;

    DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_faculty);
        etId = (AppCompatEditText)findViewById(R.id.et_id);
        etName = (AppCompatEditText)findViewById(R.id.et_name);
        etRole = (AppCompatEditText)findViewById(R.id.et_role);
        btnCreate = (Button)findViewById(R.id.btn_create);
        etPassword = (AppCompatEditText)findViewById(R.id.et_password);

        databaseReference = FirebaseDatabase.getInstance().getReference().child(Const.PATH_USER).child(Const.FACULTY);



        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initiate();
            }});

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Create Faculty");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return false;
    }
    public void initiate(){
        final Faculty faculty = new Faculty();
        faculty.setId(etId.getText().toString());
        faculty.setName(etName.getText().toString());
        faculty.setRole(etRole.getText().toString());
        faculty.setPassword(etPassword.getText().toString());

        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Loading");
        dialog.show();
        databaseReference
                .orderByChild("id")
                .equalTo(faculty.getId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        dialog.dismiss();
                        if(dataSnapshot.getValue()==null){
                            createFaculty(faculty);
                        }else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(CreateFacultyActivity.this);
                            builder.setTitle("Error");
                            builder.setMessage("Already registered!");
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builder.show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        dialog.dismiss();
                        AlertDialog.Builder builder = new AlertDialog.Builder(CreateFacultyActivity.this);
                        builder.setTitle("Error");
                        builder.setMessage(databaseError.getMessage());
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.show();
                    }
                });
    }



    private void createFaculty(Faculty faculty) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Loading");
        dialog.show();
        faculty.key = databaseReference.push().getKey();
        databaseReference.child(faculty.key)
                .setValue(faculty)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dialog.dismiss();
                        AlertDialog.Builder builder = new AlertDialog.Builder(CreateFacultyActivity.this);
                        builder.setTitle("Error");
                        builder.setMessage(e.getMessage());
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builder.show();
                    }
                })
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        dialog.dismiss();
                        AlertDialog.Builder builder = new AlertDialog.Builder(CreateFacultyActivity.this);
                        builder.setTitle("Success");
                        builder.setMessage("Your account created successfully");
                        builder.setCancelable(false);
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }
                        });
                        builder.show();
                    }
                });
    }
}
