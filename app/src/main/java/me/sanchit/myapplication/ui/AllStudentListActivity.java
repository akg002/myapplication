package me.sanchit.myapplication.ui;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import me.sanchit.myapplication.adapter.StudentListAdapter;
import me.sanchit.myapplication.Const;
import me.sanchit.myapplication.R;
import me.sanchit.myapplication.interfaces.OnClickListener;
import me.sanchit.myapplication.model.Student;

public class AllStudentListActivity extends AppCompatActivity {
    static String TAG = AllStudentListActivity.class.getSimpleName();
    RecyclerView recyclerView;
    DatabaseReference ref;
    ArrayList<Student> studentsList = new ArrayList<>();
    private StudentListAdapter studentListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_student_list);
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        ref = FirebaseDatabase.getInstance().getReference();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        studentListAdapter = new StudentListAdapter(this, studentsList, new OnClickListener() {
            @Override
            public void onClick(int pos) {
                showStudentInfo(pos);
            }
        });

        setUpStudentList();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Students");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return false;
    }


    private void showStudentInfo(int pos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AllStudentListActivity.this);
        builder.setView(R.layout.dialog_student_details);
        final AlertDialog dialog = builder.create();
        dialog.show();
        final Student student = studentsList.get(pos);
        ((EditText)dialog.findViewById(R.id.et_id)).setText(student.getId());
        ((EditText)dialog.findViewById(R.id.et_name)).setText(student.getName());
        ((EditText)dialog.findViewById(R.id.et_branch)).setText(student.getBranch());
        ((EditText)dialog.findViewById(R.id.et_year)).setText(""+student.getYr());
        final ProgressBar progressBar = (ProgressBar)dialog.findViewById(R.id.progressBar);
        progressBar.setIndeterminate(true);
        ((Button)dialog.findViewById(R.id.btn_accept)).setVisibility(View.GONE);
        ((Button)dialog.findViewById(R.id.btn_reject)).setText("Delete");
        ((Button)dialog.findViewById(R.id.btn_reject)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put( student.key, student);
                ref.child(Const.PATH_USER).child(Const.STUDENT).child(student.key).removeValue(new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                        progressBar.setVisibility(View.GONE);
                        dialog.dismiss();
                        if (databaseError !=null)
                            Log.d(TAG,databaseError.toString());
                    }
                });
            }
        });


    }
    private void setUpStudentList() {
        recyclerView.setAdapter(studentListAdapter);
        ref.child(Const.PATH_USER).child(Const.STUDENT)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        Log.d(TAG, "Child Added"+dataSnapshot.toString());
                        Student student = dataSnapshot.getValue(Student.class);
                        student.key = dataSnapshot.getKey();
                        studentsList.add(student);
                        studentListAdapter.notifyItemInserted(studentsList.size()-1);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        Log.d(TAG,"Child changed "+ dataSnapshot.toString());
                        Student student = dataSnapshot.getValue(Student.class);
                        int pos = studentsList.lastIndexOf(student);
                        studentsList.set(pos,student);
                        studentListAdapter.notifyItemChanged(pos);

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        Log.d(TAG,"Child removed "+ dataSnapshot.toString());
                        Student student = dataSnapshot.getValue(Student.class);
                        int pos = studentsList.lastIndexOf(student);
                        studentsList.remove(pos);
                        studentListAdapter.notifyItemRemoved(pos);
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                        Log.d(TAG, dataSnapshot.toString());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.d(TAG, databaseError.toString());
                    }
                });
    }
}
