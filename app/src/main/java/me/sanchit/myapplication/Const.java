package me.sanchit.myapplication;

/**
 * Created by AKG002 on 05-03-2017.
 */

public class Const {
    public static final String TYPE = "type";
    public static final String APPLICATIONS = "applications";
    public static final String TIMESTAMP = "timestamp";
    public static final String USER_LOGGED_IN = "USER_LOGGED_IN";
    public static String PATH_USER = "users";
    public static String ADMIN = "admin";
    public static String FACULTY = "faculty";
    public static String STUDENT = "student";
    public static String STATUS = "status";
    public static String STATUS_ACCEPTED = "ACCEPTED";
    public static String STATUS_REJECTED = "REJECTED";
    public static String STATUS_PENDING = "PENDING";

    public static String KEY ="key";
    public static String STUDENT_NAME="name";
    public static String ID ="id";
    public static String STUDENT_YEAR="year";
    public static String STUDENT_BRANCH="branch";



}
