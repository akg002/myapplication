package me.sanchit.myapplication.reciever;

/**
 * Created by akg002 on 28/3/17.
 */

        import android.content.BroadcastReceiver;
        import android.content.Context;
        import android.content.Intent;

        import me.sanchit.myapplication.services.FirebaseBackgroundService;

/**
 * Start the service when the device boots.
 *
 */
public class StartFirebaseAtBoot extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context,FirebaseBackgroundService.class));
    }
}