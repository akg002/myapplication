package me.sanchit.myapplication.services;

/**
 * Created by akg002 on 28/3/17.
 */


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import me.sanchit.myapplication.Const;
import me.sanchit.myapplication.R;
import me.sanchit.myapplication.model.ApplicationDetail;
import me.sanchit.myapplication.model.Faculty;
import me.sanchit.myapplication.model.Student;
import me.sanchit.myapplication.ui.AdminDashboardActivity;
import me.sanchit.myapplication.ui.FacultyDashboardActivity;
import me.sanchit.myapplication.ui.SplashActivity;
import me.sanchit.myapplication.ui.StudentDashboardActivity;

public class FirebaseBackgroundService extends Service {

    private DatabaseReference ref ;
    private ChildEventListener handler;
    private SharedPreferences sharedPreferences;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (handler!=null && ref !=null) {
            ref.removeEventListener(handler);
            handler = null;
        }
        Log.d("NotificationService","stopped");

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("NotificationService","started");
        sharedPreferences = getSharedPreferences(getString(R.string.app_name),MODE_PRIVATE);
        ref= FirebaseDatabase.getInstance().getReference();
        if (sharedPreferences.getBoolean(Const.USER_LOGGED_IN,false)) {
            if (sharedPreferences.getString(Const.TYPE,null).equalsIgnoreCase(Const.ADMIN)) {
                Log.d("NotificationService","admin");
               handler = new ChildEventListener() {
                   @Override
                   public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                       Student student = dataSnapshot.getValue(Student.class);
                       if (student.getStatus().equalsIgnoreCase(Const.STATUS_PENDING)){
                           Intent intent = new Intent(getApplicationContext(),AdminDashboardActivity.class);
                           postNotif("Student Registered :"+student.getName(), student.getId()+" "+student.getYr()+"YR "+student.getBranch(), intent);
                       }
                   }

                   @Override
                   public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                   }

                   @Override
                   public void onChildRemoved(DataSnapshot dataSnapshot) {

                   }

                   @Override
                   public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                   }

                   @Override
                   public void onCancelled(DatabaseError databaseError) {

                   }
               };
               ref.child(Const.PATH_USER).child(Const.STUDENT).addChildEventListener(handler);

            }else if (sharedPreferences.getString(Const.TYPE,null).equalsIgnoreCase(Const.FACULTY)) {
                Log.d("NotificationService","faculty");
                Gson gson = new Gson();
                final Faculty faculty = gson.fromJson(sharedPreferences.getString(Const.FACULTY,null),Faculty.class);
                handler = new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        ApplicationDetail item = dataSnapshot.getValue(ApplicationDetail.class);
                        if (item.getStatus().equalsIgnoreCase(Const.STATUS_PENDING)) {
                            Intent intent = new Intent(getApplicationContext(),FacultyDashboardActivity.class);
                            intent.putExtra(Const.FACULTY,faculty);
                            postNotif("Application posted :"+item.getStudentName(), item.getSubject(), intent);

                        }else {}
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                };
                ref.child(Const.PATH_USER).child(Const.FACULTY).child(faculty.key).child(Const.APPLICATIONS).addChildEventListener(handler);

            }else if (sharedPreferences.getString(Const.TYPE,null).equalsIgnoreCase(Const.STUDENT)) {
                Log.d("NotificationService","student");
                Gson gson = new Gson();
                final Student student = gson.fromJson(sharedPreferences.getString(Const.STUDENT,null),Student.class);
                handler = new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                        ApplicationDetail item = dataSnapshot.getValue(ApplicationDetail.class);
                        if (item.getStatus().equalsIgnoreCase(Const.STATUS_ACCEPTED)) {
                            Intent intent = new Intent(getApplicationContext(),StudentDashboardActivity.class);
                            intent.putExtra(Const.STUDENT,student);
                            postNotif("Application accepted :"+item.getFacultyName(), item.getSubject(), intent);
                        }else  if (item.getStatus().equalsIgnoreCase(Const.STATUS_REJECTED)) {
                            Intent intent = new Intent(getApplicationContext(),StudentDashboardActivity.class);
                            intent.putExtra(Const.STUDENT,student);
                            postNotif("Application rejected :"+item.getFacultyName(), item.getSubject(), intent);
                        }else {}
                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                };
                ref.child(Const.PATH_USER).child(Const.STUDENT).child(student.key).child(Const.APPLICATIONS).addChildEventListener(handler);

            }
        }

    }

    private void postNotif(String title,String text,Intent notificationIntent) {
        if (!sharedPreferences.getBoolean(Const.USER_LOGGED_IN,false)) {
            return;
        }
        Log.d("NotificationService",title);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int icon = R.drawable.ic_app_icon;
       Notification.Builder builder = new Notification.Builder(getApplicationContext());
        builder.setContentTitle(title);
        builder.setContentText(text);
        builder.setSmallIcon(icon);
        builder.setAutoCancel(true);
        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(),(int) SystemClock.currentThreadTimeMillis(), notificationIntent, 0);
        builder.setContentIntent(contentIntent);
        Notification notification = builder.build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        notification.defaults |= Notification.FLAG_SHOW_LIGHTS;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        mNotificationManager.notify((int) SystemClock.currentThreadTimeMillis(), notification);
    }

}